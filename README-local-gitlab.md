# start.kubermatic.io - Manual Deployment Steps

Here's the list of exact steps to be followed if you want to validate everything yourself
(without having that delivered with GitLab CI/CD pipeline).

We will make use of GitLab Terraform State.

This can be handy for testing everything locally, understanding the whole flow and to be able to customize
for your needs later on.

## Preparation

### Required Tools

 * `git`
 * `terraform` (1.0.4+)
 * `kubeone` (1.3.0+)
 * `sops` (3.7.1+)
 * `flux` v2 (0.16.1+)

### Setup env.variables

```bash
# from AWS console or CLI
export AWS_ACCESS_KEY_ID=<AWS_ACCESS_KEY_ID>
# from AWS console or CLI      
export AWS_SECRET_ACCESS_KEY=<AWS_SECRET_ACCESS_KEY>
# from GitLab settings
export GITLAB_TOKEN=<GITLAB_TOKEN>
# your GitLab owner / username / organization
export GITLAB_OWNER=<GITLAB_OWNER>
# your GitLab repository name
export GITLAB_REPOSITORY=<GITLAB_REPOSITORY>
```

### Prepare GitLab repository

Create a project on your GitLab instance with `<GITLAB_OWNER>` and `<GITLAB_REPOSITORY>`.

```bash
git init
git checkout -b main
# do not add a .gitlab-ci.yml file!
git add README.md .gitignore flux/ kubermatic/ terraform/ kubeone/
git commit -m "Initial setup for KKP on Autopilot"
git remote add origin git@gitlab.com:$GITLAB_OWNER/$GITLAB_REPOSITORY.git
git push -u origin main
```

## Deployment

### Prepare SSH key-pair

```bash
ssh-keygen -t rsa -b 4096 -C "<ADMIN_EMAIL>"
```

You will be prompted to provide a key location, e.g. `~/.ssh/k8s_rsa`, if you choose different location
make sure to update the `ssh_public_key_file` variable in terraform!

### Prepare infrastructure with Terraform

```bash
cd terraform/aws
export GITLAB_PROJECT_ID=<GITLAB_PROJECT_ID> # get project ID from GitLab UI or API
export GITLAB_TF_STATE_NAME=kkp              # name of the Gitlab Terraform state
export GITLAB_USERNAME=<GITLAB_USERNAME>     # your GitLab user associated with GITLAB_TOKEN
terraform init \
  -backend-config="address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_TF_STATE_NAME}" \
  -backend-config="lock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_TF_STATE_NAME}/lock" \
  -backend-config="unlock_address=https://gitlab.com/api/v4/projects/${GITLAB_PROJECT_ID}/terraform/state/${GITLAB_TF_STATE_NAME}/lock" \
  -backend-config="username=${GITLAB_USERNAME}" \
  -backend-config="password=${GITLAB_TOKEN}" \
  -backend-config="lock_method=POST" \
  -backend-config="unlock_method=DELETE" \
  -backend-config="retry_wait_min=5"
eval `ssh-agent`
ssh-add ~/.ssh/k8s_rsa
terraform apply
terraform output -json > output.json
cd ../..
```

### Initialize k8s cluster with KubeOne

```bash
cd kubeone
kubeone apply -m kubeone.yaml -t ../terraform/aws/output.json -y -v
export KUBECONFIG=`pwd`/kkp-demo-gitlab-cds-kubeconfig
cd ..
```

### Deploy KKP

First we need to decrypt the kkp-config and helm values, put the secret key from secrets.md in `.age.key`.

```bash
export SOPS_AGE_KEY_FILE=`pwd`/.age.txt
cd kubermatic
sops -d -i kubermatic-configuration.yaml
sops -d -i values.yaml
export VERSION=v2.18.0
wget https://github.com/kubermatic/kubermatic/releases/download/${VERSION}/kubermatic-ce-${VERSION}-linux-amd64.tar.gz
tar -xzvf kubermatic-ce-${VERSION}-linux-amd64.tar.gz
./kubermatic-installer deploy --config kubermatic-configuration.yaml --helm-values values.yaml --storageclass aws
cd ..
```

### Prepare other resources for KKP and Flux

```bash
kubectl create secret generic kubeconfig-cluster -n kubermatic --from-file=kubeconfig=`pwd`/kubeone/kkp-demo-gitlab-cds-kubeconfig --dry-run=client -o yaml | kubectl apply -f -
kubectl create secret generic kubermatic-values -n kubermatic --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
# Values for helm releases are able to read secret only from same namespace so we need values in all NS
kubectl create secret generic kubermatic-values -n logging --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
kubectl create secret generic kubermatic-values -n monitoring --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
kubectl create secret generic kubermatic-values -n iap --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
kubectl create secret generic kubermatic-values -n kube-system --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
kubectl create secret generic kubermatic-values -n minio --from-file=values.yaml=`pwd`/kubermatic/values.yaml --dry-run=client -o yaml | kubectl apply -f -
cat .age.txt | kubectl -n kubermatic create secret generic sops-age --from-file=age.agekey=/dev/stdin --dry-run=client -o yaml | kubectl apply -f -
```

### Update DNS records

At this point, you should manage the DNS records the Kubermatic Kubernetes platform.

See the related [documentation](https://docs.kubermatic.com/kubermatic/master/guides/installation/install_kkp_ce/#create-dns-records).

### Initialize Flux v2

```bash
flux bootstrap gitlab --owner=$GITLAB_OWNER --repository=$GITLAB_REPOSITORY \
  --branch=main --personal=true --path flux/clusters/master
```

Your Kubermatic Kubernetes Platform is ready to use now, look around
all deployed services and custom resources in Kubernetes.

More cheat sheets to follow are summarized in [documentation](https://docs.kubermatic.com/kubermatic/master/startio/cheat_sheets).
